# CoCoRenum

CoCoRenum is a VS Code extension that renumbers BASIC line numbers.

Renumbers the currently opened BASIC file in the same fashion that old RENUM statement works. Available from the Command Palette or Ctrl+Shift+r (Mac: Cmd+Shift+r)