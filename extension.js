const vscode = require('vscode');
const workbenchConfig = vscode.workspace.getConfiguration('cocorenum');

var renumber_increment = workbenchConfig.get('renumberIncrement');

const tools = require('./tools');
const path = require('path');

class Bas_Line {
    // object to represent a line of basic code,
    // with original and new line numbers

    constructor(line_num, line_str) {
        this._line_num = line_num;
        this._line_str = line_str;
    }
    set new_line_num(new_line_num) {
        this._new_line_num = new_line_num;
    }
    set line_str(new_line_str) {
        this._line_str = new_line_str;
    }
    get line_str() {
        return this._line_str;
    }
    get new_str() {
        //return the line with new line number

        //if the line doesn't start with a line number, insert the new number
        try {
            var line_num = parseInt(this.line_str.match(/^\d+/)[0]);
        }
        catch (ex) {
            return this.new_line_num + ' ' + this.line_str;
        }

        return this.line_str.replace(this.line_num, this.new_line_num);

    }
    get line_num() {
        return this._line_num;
    }
    get new_line_num() {
        return this._new_line_num;
    }
}

function Renumber(lines, keyword) {
    lines.forEach(line => {
        let goto = line.line_str.indexOf(keyword);
        if (goto > -1) {
            var match = new RegExp(keyword + '\\ ?\\d+', 'g');
            let gonum = line.line_str.match(match);
            if (gonum != null && gonum.length > 0) {
                for (var x = 0; x < gonum.length; x++) {
                    var m = gonum[x].match(/\d+/);
                    for (var y = 0; y < m.length; y++) {
                        let line_num = m[y];
                        let new_num = GetNewLineNum(lines, line_num);
                        let newgonum = gonum[x].replace(line_num, new_num);
                        if (new_num != 0)
                            line.line_str = line.line_str.replace(gonum, newgonum);
                        else
                            vscode.window.showInformationMessage('Line not found: ' + line_num);
                    }
                }
            }
        }
    });
    return lines;
}

function GetNewLineNum(lines, original_line_num) {
    var return_num = 0;
    lines.forEach(line => {
        if (line.line_num == original_line_num) {
            return_num = line.new_line_num;
        }
    });
    return return_num;
}

async function getRenumberSelectionStartNum(placeholder) {
    let userInputWindow = await vscode.window.showInputBox({ placeHolder: placeholder, prompt: 'Renumber selected lines starting with' });
    return userInputWindow;
}

function activate(context) {
    let renumber = vscode.commands.registerCommand('cocorenum.renumber', async () => {
        if (path.extname(vscode.window.activeTextEditor.document.fileName).toUpperCase() == '.BAS') {


            let editor = vscode.window.activeTextEditor;
            if (!editor) {
                return; // No open text editor
            }

            // vscode.window.showInputBox({ prompt: 'Here is the prompt' });
            //window.showInformationMessage('Hello World!');


            var linelist = []; //list of Bas_Line objects...one for each line of basic code

            let edit = new vscode.WorkspaceEdit();
            var ls;

            var st = vscode.window.activeTextEditor.selection.start.line;
            var en = vscode.window.activeTextEditor.selection.end.line;

            if (en == NaN || en <= st) {
                st = 0;
                en = editor.document.lineCount - 1;
            }
            else {
                //show the dialog

                //let renumberSelected = vscode.commands.registerCommand('cocorenum.renumberSelected', async () => {
                    // if (path.extname(vscode.window.activeTextEditor.document.fileName).toUpperCase() == '.BAS') {

                    //     let editor = vscode.window.activeTextEditor;
                    //     if (!editor) {
                    //         return; // No open text editor
                    //     }

                        var start_sel = editor.selection.start;
                        var end_sel = editor.selection.end;

                        var renumberSelectionStartNum = await getRenumberSelectionStartNum("7");
                        st = renumberSelectionStartNum;
                        en = st + 5; // var selNum = renumberSelectionStartNum;
                        //TODO: I think I'm doing the actual editor line numbere here, not the BASIC line number
                    }
                //});

                if (en < editor.document.lineCount)
                    en++;
            }

            //build array of old and new line numbers
            for (ls = st; ls < en; ls++) {
                let line = editor.document.lineAt(ls);
                var line_num = 1;
                try {
                    line_num = parseInt(line.text.match(/^\d+/)[0]);
                }
                catch (ex) {

                }

                let line_str = line.text.substring(line_num.length);
                var l = new Bas_Line(line_num, line_str);
                linelist.push(l);
            }

            var line_start = renumber_increment;
            linelist.forEach(line => {
                line.new_line_num = line_start;
                line_start += renumber_increment;
            });

            Renumber(linelist, "GOTO");
            Renumber(linelist, "GOSUB");
            Renumber(linelist, "THEN");
            Renumber(linelist, "ELSE");

            for (ls = st; ls < en; ls++) {
                let line = editor.document.lineAt(ls);
                //if (line.text.trimRight().length > 0) {
                //console.log("LINE: " + linelist[ls].new_str);
                edit.replace(editor.document.uri, line.range, linelist[ls - st].new_str);
                //}
            }

            vscode.workspace.applyEdit(edit);
            vscode.window.showInformationMessage('Code Renumber Complete!');
        }
    

    );

    context.subscriptions.push(renumber);

    // let renumberSelected = vscode.commands.registerCommand('cocorenum.renumberSelected', async () => {
    //     if (path.extname(vscode.window.activeTextEditor.document.fileName).toUpperCase() == '.BAS') {

    //         let editor = vscode.window.activeTextEditor;
    //         if (!editor) {
    //             return; // No open text editor
    //         }

    //         // var start_sel = editor.selection.start;
    //         // var end_sel = editor.selection.end;

    //         var renumberSelectionStartNum = await getRenumberSelectionStartNum("7");
    //     }

    //     //if no lines selected, show a warning and exit

    //     //if lines are selected, ask for the beginning renum number

    //     //check to see if the selected number of lines will fall outside of existing line numbers. If not, show an error, else renumber the block


    // });

    // context.subscriptions.push(renumberSelected);
}
exports.activate = activate;

function deactivate() {
}
exports.deactivate = deactivate;